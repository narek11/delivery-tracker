import {StatusBar} from 'expo-status-bar';
import React, {useEffect} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux'
import store from './src/redux/store'
import Navigation from './src/navigation';
import {askLocationPermission} from "./src/services/LocationService";

const App = () => {
    useEffect(() => {
        (async () => {
            await askLocationPermission()
        })()

    }, [])
    return (
        <Provider store={store}>
            <SafeAreaProvider>
                <Navigation />
                <StatusBar />
            </SafeAreaProvider>
        </Provider>
    );
}

export default App;
