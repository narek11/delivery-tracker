export interface DeliveryType {
    id: number;
    customer: string;
    latitude: number;
    longitude: number;
    zipCode: string;
    city: string;
    address: string;
}


export interface FinishDeliveryType {
    id: number;
    status: 'in-progress' | 'delivered' | 'undelivered';
    latitude: number;
    longitude: number;
}
