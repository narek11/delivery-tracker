import * as React from 'react'
import {FlatList, Text, View, TouchableOpacity} from 'react-native'
import {Ionicons} from '@expo/vector-icons'
import {Separator} from '../Layout'
import {DeliveryType} from "../../models/DeliveriesModel";

interface DeliveriesListProps {
    list: DeliveryType[];
    onItemPress: (id: number) => void
}

interface DeliveryProps {
    id: number;
    onPress: () => void,
    customer: string
}

export const Delivery = React.memo(({id, onPress, customer}: DeliveryProps): JSX.Element => (
        <TouchableOpacity key={id} style={styles.deliveryContainer} onPress={onPress} accessible={true}
                          accessibilityLabel='Delivery list item button'>
            <Text style={styles.customer}>{customer}</Text>
            <Ionicons name='chevron-forward' size={26}></Ionicons>
        </TouchableOpacity>
    )
)

export const DeliveriesList = (props: DeliveriesListProps): JSX.Element => (
    <FlatList
        data={props.list}
        contentContainerStyle={styles.container}
        renderItem={({item: {id, customer}, index}) => (
            <Delivery
                id={id}
                onPress={() => props.onItemPress(id)}
                customer={customer}
            />
        )
        }
        ItemSeparatorComponent={() => <Separator />}
        showsVerticalScrollIndicator={false}
    />
)

const styles = {
    container: {
        marginHorizontal: 10
    },
    deliveryContainer: {
        borderWidth: 1,
        borderColor: 'grey',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    customer: {
        fontSize: 17,
        padding: 10
    }
}

export default DeliveriesList
