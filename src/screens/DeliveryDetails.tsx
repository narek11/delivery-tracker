import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux'
import {finishDelivery} from "../redux/actions/deliveries";
import {getCurrentLocation} from "../services/LocationService";

const DeliveryDetailsWrapper: React.FC = ({route}) => {
    const {id} = route.params
    const delivery = useSelector(
        (s) => s
            .deliveries.list
            .find(dv => dv.id === id) || {}
    )

    const {status, latitude, longitude} = delivery

    const {id: id_inProgress, customer: customer_inProgress} = useSelector(
        (s) => s
            .deliveries.list
            .find(dv => dv.status === 'in-progress')) || {}

    return (
        <View style={styles.container}>
            <Choose>
                <When condition={status === 'delivered' || status === 'undelivered'}>
                    <Done customer={delivery.customer} status={status} />
                </When>
                <When condition={id_inProgress && id !== id_inProgress}>
                    <InProgress customer={customer_inProgress} />
                </When>
                <When condition={status === 'in-progress'}>
                    <MarkDelivered id={id} latitude={latitude} longitude={longitude} />
                </When>
                <Otherwise>
                    <MakeActive id={id} latitude={latitude} longitude={longitude} />
                </Otherwise>
            </Choose>
        </View>
    );
}

interface InProgressType {
    customer: string;
}

const InProgress = ({customer}: InProgressType): JSX.Element => {
    return (
        <View>
            <Text>Delivery for {customer} in progress!</Text>
        </View>
    )
}

type statusType = 'delivered' | 'undelivered' | 'in-progress'

interface DoneType {
    customer: string;
    status: statusType
}
const Done = ({customer, status}: DoneType): JSX.Element => {
    let text = status === 'delivered'
        ? `Package for ${customer} has been delivered!`
        : `Package for ${customer} marked as undelivered!`
    return (
        <View>
            <Text>{text}</Text>
        </View>
    )
}

interface MarkDeliveredType {
    id: number;
    latitude: number;
    longitude: number;
}

const MarkDelivered = ({id, latitude, longitude}: MarkDeliveredType): JSX.Element => {
    const dispatch = useDispatch()
    const onDelivered = async () => {
        const coords = await getCurrentLocation()
        const {lat, lon} = coords || {}

        dispatch(finishDelivery({id, status: 'delivered', latitude: lat || latitude, longitude: lon || longitude}))
    }
    const onUnDelivered = async () => {
        const coords = await getCurrentLocation()
        const {lat, lon} = coords || {}

        dispatch(finishDelivery({id, status: 'undelivered', latitude: lat || latitude, longitude: lon || longitude}))
    }


    return (
        <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={styles.deliveredButton} onPress={onDelivered}>
                <Text style={styles.deliveredButtonText}>Delivered</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.undeliveredButton} onPress={onUnDelivered}>
                <Text style={styles.undeliveredButtonText}>Undelivered</Text>
            </TouchableOpacity>
        </View>
    )
}

interface MarkDeliveredType {
    id: number;
    latitude: number;
    longitude: number;
}
const MakeActive = ({id, latitude, longitude}: MarkDeliveredType): JSX.Element => {
    const dispatch = useDispatch()
    const onMakeActive = () => {
        dispatch(finishDelivery({id, status: 'in-progress', latitude, longitude}))
    }

    return (
        <View>
            <TouchableOpacity style={styles.makeActiveButton} onPress={onMakeActive}>
                <Text style={styles.makeActiveText}>Make active</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
        container: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
        },
        makeActiveButton: {
            padding: 10,
            backgroundColor: 'grey'
        },
        makeActiveText: {
            color: 'white',
        },
        deliveredButton: {
            padding: 10,
            backgroundColor: 'green'
        },
        deliveredButtonText: {
            color: 'white'
        },
        undeliveredButton: {
            padding: 10,
            backgroundColor: 'red'
        },
        undeliveredButtonText: {
            color: 'white'
        }

    }
);

export default DeliveryDetailsWrapper;
