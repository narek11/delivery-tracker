import React, {useEffect, useState} from 'react';
import {ActivityIndicator, StyleSheet, Text, View, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux'
import DeliveriesList from '../components/Deliveries/DeliveriesList'
import {getDeliveries} from '../redux/actions/deliveries'
import {Routes} from "../navigation/routes";

const DeliveriesScreen: React.FC = ({navigation}) => {
    const [loading, setLoading] = useState(true)
    const dispatch = useDispatch()
    const {list: dvs} = useSelector((s) => s.deliveries)
    useEffect(() => {
        const fetch = async () => {
            await dispatch(getDeliveries())
            setLoading(false)
        }

        fetch()
    }, [])

    if (loading) {
        return (<ActivityIndicator />)
    }

    return (
        <View style={styles.container}>
            <DeliveriesList list={dvs} onItemPress={(id) => navigation.navigate(Routes.DeliveryDetails, {id})} />
        </View>
    );
}


const styles = StyleSheet.create({
        container: {
            flex: 1
        },

    }
);

export default DeliveriesScreen;
