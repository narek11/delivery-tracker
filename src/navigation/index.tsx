import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import DeliveriesScreen from "../screens/DeliveriesScreen";
import DeliveryDetails from "../screens/DeliveryDetails";
import {Routes} from './routes'

const Stack = createNativeStackNavigator();
const Navigation = (): JSX.Element => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name={Routes.Deliveries} component={DeliveriesScreen} options={{title: 'Deliveries'}} />
                <Stack.Screen name={Routes.DeliveryDetails} component={DeliveryDetails} options={{title: 'Delivery Details'}} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default Navigation;
