import {Alert} from 'react-native';
import * as Location from 'expo-location';

export const askLocationPermission = async (): Promise<string | void> => {
    let {status} = await Location.requestForegroundPermissionsAsync();

    if (status !== 'granted') {
        Alert.alert('Need location access to record delivery location!')
        return;
    }

    return status
}

interface CoordsType {
    lat: number;
    lon: number;
}

export const getCurrentLocation = async (): Promise<CoordsType | null> => {
    let coords = null

    try {
        const {coords: {latitude, longitude}} = await Location.getCurrentPositionAsync({});
        coords = {lat: latitude, lon: longitude}
    } catch (e) {
        console.log('read location error', e)
    }

    return coords
}
