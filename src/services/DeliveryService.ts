import {URLS} from '../constants/api';
import {errorHandler, responseHandler} from '../utils';
import {DeliveryType, FinishDeliveryType} from '../models/DeliveriesModel';
import {errorType} from '../utils'

export const getDeliveries = (): Promise<DeliveryType[] | errorType> => {
    return fetch(URLS.DELIVERIES)
        .then(responseHandler)
        .catch(errorHandler);
};


export const finishDelivery = ({
                                   id,
                                   status,
                                   latitude,
                                   longitude
                               }: FinishDeliveryType): Promise<FinishDeliveryType | errorType> => {
    const requestBody = JSON.stringify({deliveryId: id, status, latitude, longitude});
    return fetch(URLS.DELIVERY_FINISH, {method: 'POST', body: requestBody,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(responseHandler)
        .catch(errorHandler);
}
