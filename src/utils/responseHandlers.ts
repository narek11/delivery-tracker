export type responseType = any | { message: 'Invalid JSON Response', error: any }
export type errorType = { message: 'Request Failed!', error: any }

export const responseHandler = (res: any): responseType => {
    try {
        return res.json();
    } catch (e) {
        return {
            error: e,
            message: 'Invalid JSON Response',
        };
    }
};

export const errorHandler = (e: any): errorType => {
    console.log(e);
    return {
        error: e,
        message: 'Request Failed!',
    };
};
