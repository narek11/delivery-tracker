import React from 'react'
import {Provider} from 'react-redux';
import store from '../redux/store';

export const withProvider = (Component: React.ComponentClass): Function => {
    return () => (
        <Provider store={store}>
            <Component />
        </Provider>
    )
}
