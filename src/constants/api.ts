const BASE_URL = 'https://60e84194673e350017c21844.mockapi.io/api';

export const URLS = {
    DELIVERIES: `${BASE_URL}/deliveries`,
    DELIVERY_FINISH: `${BASE_URL}/finishDelivery`,
};
