import * as aT from '../actions/ActionTypes'
import {DeliveryType} from "../../models/DeliveriesModel";

const initState = {
    list: []
}

const deliveriesReducer: any = (state = initState, action) => {
    switch (action.type) {
        case aT.GET_DELIVERIES: {
            const {list} = action.payload

            return {...state, list}
        }
        case aT.FINISH_DELIVERY: {
            const {delivery} = action.payload;
            const list = state.list.map((dv: DeliveryType) => {

                if (dv.id === delivery.deliveryId) {
                    return {
                        ...dv,
                        status: delivery.status,
                        latitude: delivery.latitude,
                        longitude: delivery.longitude,
                    }
                }


                return dv

            })

            return {
                ...state,
                list
            }

        }
    } //switch

    return state
}

export default deliveriesReducer
