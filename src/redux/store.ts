import {createStore, applyMiddleware, combineReducers} from '@reduxjs/toolkit'
import ReduxThunk from 'redux-thunk'
import deliveriesReducer from '../redux/reducers/deliveries'

const reducers = combineReducers({
    deliveries: deliveriesReducer,
})

let middlewares = [ReduxThunk]

let store = createStore(reducers, applyMiddleware(ReduxThunk))

export default store;
