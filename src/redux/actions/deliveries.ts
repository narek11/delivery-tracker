import * as aT from './ActionTypes'
import {getDeliveries as getDeliveriesAsync, finishDelivery as finishDeliveryAsync} from '../../services/DeliveryService'
import {FinishDeliveryType} from '../../models/DeliveriesModel'
const getDeliveries = () => {
    try {
        return async (dispatch: any) => {
            const list = await getDeliveriesAsync()

            dispatch({
                type: aT.GET_DELIVERIES,
                payload: {list}
            })

            return Promise.resolve()

        }
    } catch (e) {
        console.log('error', e)
    }
}//getDeliveries


const finishDelivery = ({id, status, latitude, longitude}: FinishDeliveryType) => {
    try {
        return async (dispatch: any) => {
            const updatedDelivery = await finishDeliveryAsync({id, status, latitude, longitude})

            dispatch({
                type: aT.FINISH_DELIVERY,
                payload: {delivery: updatedDelivery}
            })

            return Promise.resolve()

        }
    } catch (e) {
        console.log('error', e)
    }
}//getDeliveries

export {getDeliveries, finishDelivery}
