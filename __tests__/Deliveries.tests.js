import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import DeliveriesList from '../src/components/Deliveries/DeliveriesList'
import DeliveriesScreen from '../src/screens/DeliveriesScreen';
import deliveries from '../src/data/deliveries';
import { withProvider } from '../src/utils';

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(deliveries),
  })
);
describe('DeliveriesList component', () => {

  it('DeliveriesScreen first 10 deliveries to be rendered in FlatList', async () => {
    const DeliveriesScreen__ = withProvider(DeliveriesScreen);
    const { getAllByA11yLabel } = render(
      <DeliveriesScreen__ />
    );

    await waitFor(() => expect(getAllByA11yLabel('Delivery list item button')).toHaveLength(10));
  });

  it('DeliveriesList component first 10 deliveries to be rendered in FlatList', async () => {
    const { getAllByA11yLabel } = render(
      <DeliveriesList list={deliveries} onItemPress={() => {
      }} />
    );

    expect(getAllByA11yLabel('Delivery list item button')).toHaveLength(10);
  });

});//describe
